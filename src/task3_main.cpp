#include "DZ31.5.h"

template<typename T1, typename T2>


struct Item {
	T1 key;
	T2 value;

	Item(T1 newKey, T2 newVal) :key(newKey), value(newVal) {};
};
/*
class Item {
	T1 key;
	T2 value;
public:
	//Item(){};
	//Item(T1 newKey, T2 newVal) :key(std::to_string(newKey)), value(newVal) {};
	Item(T1 newKey, T2 newVal) :key(newKey), value(newVal) {};
	~Item() {};
};
*/
template<typename T1, typename T2>
class Dict {
	std::vector<Item<T1, T2>>list;
public:
	void add(T1 key, T2 value) {
		Item<T1,T2> newElem(key, value);
		
		list.push_back(newElem);
	}
	void remove(T1 key){
		//std::vector<std::vector<Item<T1, T2>>::iterator> del_list;

		for (std::vector<Item<T1, T2>>::iterator it = list.begin(); it != list.end();) {
			if (it->key == key){
				it = list.erase(it); //del_list.push_back(it);
			} 
			else { 
				++it;
			}
		}
	}
	void find(T1 key) {
		for (auto &it : list) {
			if (it.key == key) std::cout << it.value << std::endl;
		}
	}
	void print() {
		for (auto &it : list) {
			std::cout << "key: "<< it.key << " value: " << it.value << std::endl;
		}
	}
};


/*add � �������� ������� � ������;
remove � ������� ��� �������� � �������� ������;
print � ���������� �� ������ ��� �������� � �� �������;
find � ����� ��� �������� �� �� �����*/

void task3() {
	int key, cmd = 0;
	std::string val;
	
	setlocale(0, " ");
	std::cout << "Hello task3" << std::endl;

	Dict<int, std::string> newDict;

	while (cmd >= 0) {
		std::cout << "Chois doing with dictionary: 1 - add, 2 - remove, 3 - print, 4 - find� -1 - exit" << std::endl;
		std::cin >> cmd;
		switch (cmd)
		{
			case 1:
				std::cout << "Enter key" << std::endl;
				std::cin >> key;
				std::cout << "Enter value" << std::endl;
				std::cin >> val;
				newDict.add(key, val);
				break;
			case 2:case 4: 
				std::cout << "Enter key" << std::endl;
				std::cin >> key;
				(cmd == 2) ? newDict.remove(key) : newDict.find(key);
				break;
			case 3:
				newDict.print();
				break;
		default:
			if (cmd < 0) std::cout << "Uncknown command " << std::endl;
			break;
		}
	}
	return;
}