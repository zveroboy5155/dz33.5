#include "DZ31.5.h"

template<typename T1>
T1 arithMean(T1 *arr, int size) {
	if (arr == nullptr) return 0;
	T1 sum = *arr;
	T1 count = 1;

	for (int i = 1; i < size; i++) {
		sum += *(arr + i);
		count++;
	}
	if (count == 0) return 0;
	return sum / count;
}

template<typename T1>
bool enterArr(T1 *out_arr, int size) {
	T1 elem;

	if (!out_arr) return false;
	for (int i = 0; i < size; i++) {
		if (out_arr + i == nullptr) return false;
		std::cout << "Enter " << i + 1 << " element" << std::endl;
		std::cin >> elem;
		*(out_arr + i) = elem;
	}

	return true;
}


void task4() {
	float arr[6];// = { 1.2, 2.6, 4.3, 6.5, 3.9, 5.1 };
	
	if (!enterArr(arr, 6)) return;
	
	for (auto it : arr) {
		std::cout << it << std::endl;
	}
	std::cout << arithMean(arr, 6) <<std::endl;
}