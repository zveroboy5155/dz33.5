#include "DZ31.5.h"
#include <map>


class UnknownGoodException:public std::exception {
	const char* what() const noexcept override {
		return "Unknown article";
	}
};
class NotGoodException :public std::exception {
	const char* what() const noexcept override {
		return "goods is over";
	}
};

class usrBasket {
	std::map <int, int> goods;

public:
	usrBasket() = default;
	void add_good(int art, int count) {
		if (goods.find(art) == goods.end())
			return (void)goods.insert(std::pair<int, int>(art, count));

		goods[art] += count;
	}
	void printAll() {
		for (auto &it : goods) {
			std::cout << it.first << " :" << it.second << std::endl;
		}
	}
};

class enthShop {
	std::map <int, int> Stock;
	std::vector<usrBasket> baskets;
public:
	enthShop() {
		char cmd;

		while (1) {
			std::cout << "Added new goods on stock?(Y/N):";
			std::cin >> cmd;
			if (cmd == 'N') break;
			else if (cmd == 'Y') {
				int art, count;

				std::cout << "Enter article of item and count of them" << std::endl;
				std::cin >> art >> count;
				addInStock(art, count);
			}
			else std::cout << "Uncknown cmd, please use Y or N character" << std::endl;
		}
	}
	int addUser() {
		usrBasket newUser;

		baskets.push_back(newUser);
		return baskets.size() - 1;
	}

	void addInStock(int &art, int &count) {
		if (Stock.find(art) != Stock.end()) Stock[art] += count;
		else Stock.insert(std::pair<int, int>(art, count));
	}
	bool addInBasket(int &usrId){
		int art, count;
		char cmd;

		std::cout << "Enter article of goods: ";
		std::cin >> art;
		if (Stock.find(art) == Stock.end()) throw UnknownGoodException(); ///������� ���������� - ����������� �����
		else if (Stock[art] <= 0) throw NotGoodException();//������� ���������� - ����� ����������
		else std::cout << Stock[art] <<" items available" << std::endl;
		std::cout << "Enter count of goods: ";
		std::cin >> count;
		if (count > Stock[art]) {
			count = Stock[art];
			while(1) {
				std::cout << "In stock onli " << count << " goods, add all to bascket? (Y/N)" << std::endl;
				std::cin >> cmd;
				if (cmd == 'Y') 
					break;
				else if (cmd == 'N')
					return true;
				else std::cout << "Repeat input" << std::endl;
			}
		}
		Stock[art] -= count;
		baskets[usrId].add_good(art, count);
	}
};



void task1() {
	enthShop newShop;
	int usrId = newShop.addUser();
	char cmd;

	while (1) {
		
		std::cout << "Want you add item in your backet?(Y/N)" << std::endl;
		std::cin >> cmd;

		if (cmd == 'N') break;
		else if (cmd != 'Y') continue;
		try{ 
			newShop.addInBasket(usrId);
		}
		catch (const std::exception &x) {
			std::cout << x.what() << std::endl;
		}
		

	}
}