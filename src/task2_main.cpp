#include "DZ31.5.h"
//#include <thread>
#include <chrono>
#include <ctime>

#define DBG(code) code
enum ITEM {
	empty,
	fish,
	boot
};

class CatchFishException : public std::exception {};
class CatchBootException : public std::exception {};

class fishing {
	int gameField[9];
public:
	fishing() {
		bool FishFl = false;
		int bootsCount = 0;
		int curItem;

		std::srand(std::time(nullptr));
		for (auto &it : gameField) {
			curItem = std::rand() % 3;
			switch (curItem)
			{
			case fish:
				if (FishFl) continue;
				FishFl = true;
				break;
			case boot:
				if (bootsCount == 3) continue;
				bootsCount++;
				break;
			default:
				break;
			}
			it = curItem;
		}
	}
	void catchSmn(int &field) {
		
		if (field < 1 || field > 9) throw std::invalid_argument("Only 9 fields for fishing (1-9)");
		else if (gameField[field - 1] == fish) throw CatchFishException();
		else if (gameField[field - 1] == boot) throw CatchBootException();
	}
};


void task2() {
	fishing newGame;
	int field;
	int tryCount = 0;

	while(true){
		tryCount++;
		std::cout << "Enter number of field 1 - 9" << std::endl;
		std::cin >> field;
		try {
			newGame.catchSmn(field);
		}
		catch (CatchFishException &x) {
			std::cout << "Success!!! Trying "<< tryCount << std::endl;
			return;
		}
		catch (CatchBootException &x) {
			std::cout << "Game over (" << std::endl;
			return;
		}
		catch (std::invalid_argument &x) { 
			tryCount--;
			std::cout << x.what() << std::endl;
		}
	}
}